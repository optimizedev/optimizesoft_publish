﻿<%@ page title="About Us" language="VB" masterpagefile="~/Site.Master" autoeventwireup="false" inherits="About, App_Web_ngygm4mk" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent" >
    <style type="text/css" >
        .ClearFoat li
            {
            font-size:10pt;   float: left;
            height: 20px;
            list-style-type:square;
            margin: 10px;
            vertical-align: middle;
            }
        .ClearFoat a { text-decoration:none;}
        .ClearFoat a:hover { text-decoration:underline;}

        .about_head{mso-element: para-border-div; border: solid #099BDD 3.0pt; font-size:11.0pt;mso-border-themecolor: text2; padding: 0in 0in 0in 0in; background: #099BDD; mso-background-themecolor: text2}
        .about_detail { font-size:11.0pt;mso-ansi-font-size:13.0pt;line-height:110%;font-family:Tahoma, sans-serif"}
       </style>

<div id="Container">
    <div class="BgContent">
    <h2> 
        ระบบศูนย์บริการ 
    </h2>
    <table>
        <tr valign=top>
            <td>
 <asp:Panel ID=panel1 runat=server ScrollBars=Auto Height="490px"  style="margin-top: 0px"  >
     <a name="t1"></a> 
    <h1 class="about_head">ระบบศูนย์บริการ Optimize System™</h1>
    <p class="about_detail">
        คือ ระบบเวปคลาวด์แอพลิเคชั่น ใช้งานผ่าน Browser จัดเก็บข้อมูลด้วย SQL Server ทรงประสิทธิ์ภาพ  สามารถทำงาน ได้จากทุกสถานที่  ผ่านอินเตอร์เนท หรือตั้ง Server แบบดั้งเดิม เป็น  Local Server ใช้งานผ่านอินทราเนท.
    </p>
     <a name="t2"></a>
    <h1 class="about_head">ทำไมต้องเป็น Web Application ?</h1></h1> 
    <p class="about_detail">
       ปัจจุบันการปฏบัติงานในองค์กร ด้วยคอมพิวเตอร์ ไม่จำเป็นที่ จะต้องทำจากที่ทำงานแห่งเดียว สามารถทำงาน และติดตามงาน ได้จากทุกที่  ผู้ปฏิบัติงานไม่จำเป็นต้องประจำที่ออฟฟิส โดยระบบ Optimize System™ สามารถทำงานผ่านอุปกรณหลากหลาย เช่น PC , TABLET หรือ SMART PHONE โดยการทำงานผ่าน Web Browser จะไม่ขึ้นกับ ระบบปฏิบัติการใดๆ 
    </p>
     <a name="t3"></a> 
    <h1 class="about_head">No Instalation</h1> 
    <p class="about_detail">
        ไม่ต้องลงโปรแกรมใดๆ ก็สามารถทำงานได้ นั่นเป็น เหตุผลหลักขของ WEB APPLICATION สามารถลดค่าใช้จ่าย ในการติดตั้งอุปกรณ์ ทำงานได้ทุกที่บนโลก ที่มี INTERNET รวมทั้งยังสามารถ เปิด JOB ผ่านมือถือ ระบบแอนดรอย ด้วย แอฟพลิเคชั่นของเรา Car Maintainance™ ที่ให้ดาว์นโหลดผ่าน Google Market ได้ ฟรี!
    </p>
     <a name="t4"></a> 
    <h1 class="about_head">พร้อมด้วยระบบ CRM</h1>
    <p class="about_detail">
        Optimize System™ มาพร้อมกับระบบ CRM เพื่อเพิ่มความสะดวกในการวิเคราะห์ ความต้องการของลูกค้า และการติดตาม การให้บริการหลังการขาย ต่างๆ
    </p>
     <a name="t5"></a>
    <h1 class="about_head">เข้าใจง่าย</h1>
    <p class="about_detail">
        ด้วยการออกแบบส่วนติดต่อผู้ใช้ (User Interface) แบบมาตฐาน และเป็นมิตรกับผู้ใช้ ลดการป้อนขข้อมูลที่ ซ้ำซ้อน  มีระบบความปลอดภัยสูง มีบันทึกการทำงานของแต่ละผู้ใช้ สามารถปรับแต่ง เมนูและการเข้าถึง ของแต่ละผู้ใช้   
    </p> <a name="t6"></a>    
    <h1 class="about_head">บูรณาการข้อมูล</h1>
    <p class="about_detail">
        สามารถทำงานพร้อมกันได้ หลายบริษัท จากหลายสาขา ข้อมูลเชื่อมต่อถึงกัน เรียกดูประวัติการซ่อม ธุรกรรมต่างๆ รวมกันได้ 
    </p> <a name="t7"></a> 
    <h1 class="about_head">นำเข้าข้อมูลทีแสนสะดวก</h1>
    <p class="about_detail">        
        การทำงานสามารถเก็บข้อมูลเข้าเดต้าเบส ด้วยการพิพม์ หรือยิงผ่าน Barcode Scanner สามารถขายผ่าน JOB หรือขายผ่านหน้าร้าน  รองรับ งาน SUBLET  
    </p> <a name="t8"></a> 
    <h1 class="about_head">ระบบบริหารคงคลังแบบออโต้</h1>
    <p class="about_detail">          
        ระบบบิหารสินค้าคงคลัง MAX MIN สั่งอะไหล่แบบแนะนำการสั่งอัตโนมัติ ผ่านระบบการขอซื้อและการอนุมัติ ก่อนการทำการสั่งซื้อ และทำการรับเข้าแบบอ้าง MULTIPO 
        สามารถพิมพ์บาร์โค้ดได้เลยหลังจากรับเข้า  การทำงานสะดวกเจ้าของสามารถบริหารศูนย์ซ่อม เพื่อป้องกันการทุจิต หรือกรณีที่ ระบบการสั่งผ่านใบมัดจำสินค้าในกรณีสั่งด่วน ทำให้ไม่เกิด DEAD STOCK
    
    </p> <a name="t12"></a> 
    <h1 class="about_head">การสต็อคและเบิกสินค้า</h1>
    <p class="about_detail"> 
ระบบเตรียมการตรวจนับสต็อค ให้แบบสามารถตรวจนับบางส่วน และสามารถทำได้บ่อยๆ ในการ Freeze Stock 
และค่อยทำการตรวจนับ และทำการ อนุมัติการปรับก่อนการปรับจริงจากผู้มีอำนาจในการปรับสต็อค และสามารถนับซ้ำในบาง ITEM ที่ไม่ตรง ก่อนการปรับจริง
การเบิกจ่ายอะไหล่สามารถทำงานผ่าน PDA หรือ PC โดยการยิง BAR CODE หรือ คีย์ สามารถใช้ได้ทั้ง รหัสบาร์โค้ด  และการคีย์ด้วย PRODUCT CODE
การคิดค่าบริการ ระบบเตรียม ตารางการคิดค่าริการ สำหรับการ จัดเตรียมรหัสบริการในการคิดเงิน	
    </p> <a name="t9"></a> 
    <h1 class="about_head">การออกบิลที่หลากหลาย ครบครัน</h1>
    <p class="about_detail">  
         การบริหารบิล (INVOICE) ในการเก็บเงินลูกค้า พนักงานเก็บเงิน ไม่สามารถเปลี่ยนแปลงบิลได้เมื่อมีการพิมพ์แล้ว เป็นการป้องกันการทุจริตในกรณีเจ้าของไม่ได้บริหารเอง    ใช้การลดหนี้ในกรณีที่บิลผิด และจะทำการเปิดจ็อบให้อัตโนมัติ 
การออกบิลสามารถออกบิลได้หลายแบบ 
          <ul >
            <li>ทั้งขายสด</li>
              <li>ขายเชื่อ</li>
              <li>ขายเคลม</li>
              </ul>
            ให้ส่วนลดได้หลายแบบ การทำโปรโมชั่นในรูปแบบต่างๆ และเรายังมี Module บัญชีที่รองรับการทำงานหลังบ้าน BACK OFFICE ให้สามารถบริหารศูนย์บริการ อย่างเป็นระบบ สามารถออกงบการเงิน งบกำไรขาดทุน  งบทดลอง งบดุล เป็นมืออาชีพที่แท้จริง ผ่านรูปแบบรายงานที่หลากหลาย สามารถ Export เป็น EXCEL เพื่อนำไปวิเคราะห์ต่อในมิติอื่นๆได้ รองรับการ PAYIN รายรับประจำวันผ่าน MODULE Accounting  รองรับการบริหารลูกหนี้ เจ้าหนี้ผ่านระบบ Accounting (ขณะนี้ยังเป็น WINDOW APP)
รายงาน มีรองรับการทำงานในทุกขั้นตอน ตั้งแต่รายงาน MASTER TABLE ต่างๆ รายงานการเปิด JOB รายงานการส่งเงิน  รายงานรายได้  รายงานมัดจำสินค้า    รายงานสต็อคสินค้า  เป็นต้น

    </p> <a name="t10"></a> 
    <h1 class="about_head">รองรับหลากหลายประเภทธุรกิจ</h1>
    <p class="about_detail">  
ระบบรองรับศูนย์บริการได้ทุกขนาด เช่น
        <ul >
            <li>คาร์แคร์</li>
            <li>Quick Service</li>
            <li>ร้านยาง</li>
            <li>ร้านเคลือบแก้ว</li>
            <li>ศูนย์บริการมาตราฐาน</li>
            <li>อู่ซ่อมทั่วไป</li>
        </ul>
      

    </p>
    <h1 class="about_head">ฟีเจอร์ที่สำคัญ</h1><a name="t11"></a>
    <p class="about_detail">

          <asp:Image ID="Image1" ImageUrl="~/images/app/1.jpg" runat="server" />
          <asp:Image ID="Image2"  ImageUrl="~/images/app/2.jpg" runat="server" />
          <asp:Image ID="Image3"  ImageUrl="~/images/app/3.jpg" runat="server" />
          <asp:Image ID="Image4"  ImageUrl="~/images/app/4.jpg" runat="server" />
          <asp:Image ID="Image5"  ImageUrl="~/images/app/5.jpg" runat="server" />
          <asp:Image ID="Image6"  ImageUrl="~/images/app/6.jpg" runat="server" /> 
          <asp:Image ID="Image7"  ImageUrl="~/images/app/7.jpg" runat="server" />
          <asp:Image ID="Image8"  ImageUrl="~/images/app/8.jpg" runat="server" />
          <asp:Image ID="Image9"  ImageUrl="~/images/app/9.jpg" runat="server" />
         

    </p>
    
     </asp:Panel>
            </td>
            <td style="padding-left:5px">
                <div id="NavAbout">
                    <ul>
                        <li><img src="images/lbl_about_topic2.gif" name="navabout1" border="0" id="navabout1"></li>
                    </ul>
                    <div class="ClearFoat">
                                  <br />   
                        <ul >
                             <li><a href="#t1">ระบบศูนย์บริการ</a> </li> 
                             <li><a href="#t2">ทำไมต้อง Web Application?</a> </li>
                             <li><a href="#t3">No Instalation</a></li>
                             <li> <a href="#t4">พร้อมด้วยระบบ CRM&nbsp;&nbsp;&nbsp;&nbsp;</a> </li>
                             <li><a href="#t5">เป็นมิตรกับผู้ใช้</a> </li>
                             <li><a href="#t6">จัดเก็บข้อมูลแบบบูรณาการ</a> </li>
                             <li><a href="#t7">นำเข้าข้อมูลทีแสนสะดวก</a> </li>    
                            <li><a href="#t8">ระบบบริหารคงคลังแบบออโต้</a> </li> 
                            <li><a href="#t12">การสต็อคและเบิกสินค้า</a> </li> 
                                     
                            <li><a href="#t9">ออกบิลได้หลากหลายครบครัน</a> </li>     
                            <li><a href="#t10">รองรับหลากหลายประเภทธุรกิจ</a> </li>     
                            <li><a href="#t11">ฟีเจอร์ที่สำคัญ</a> </li>                       
                        </ul>  
                        </div>                 
                </div> 
            </td>
        </tr>    
    </table>
    </div>
</div>
</asp:Content>