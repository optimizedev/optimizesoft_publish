﻿<%@ page title="Contact Us" language="VB" masterpagefile="~/Site.Master" autoeventwireup="false" inherits="contact, App_Web_ngygm4mk" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<script type="text/javascript" language=javascript>
    function getReset(form) {
        //	document.formContact.reset();
        document.getElementById('txtname').value = "ชื่อ - นามสกุล";
        document.getElementById('txtaddress').value = "ที่อยู่";
        document.getElementById('txttelephone').value = "เบอร์โทรศัพท์";
        document.getElementById('txtemail').value = "อีเมล์";
        document.getElementById('txtmessage').value = "ข้อความ";
        document.getElementById('txtcode').value = "กรุณาใส่ผลลัพธ์ด้านบน";

    }

</script>
</asp:Content>

    <asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent" >
<asp:UpdatePanel ID="udp_contact"  runat="server" RenderMode="Inline" >
<ContentTemplate>


        <div id="Container">
      	<div class="BgContent">
            <div id="ColAddress">   
                <h1><img src="images/logo_contact.png" ></h1><%=AppSettings("address") %><br><br>            
                <img src="images/icn_letter.png" width="24" height="18" style="margin:6px 5px -4px 0;"> <a href='mailto:<%=appsettings("mail_to") %>'><%=AppSettings("mail_to")%></a><br>
                <img src="images/icn_tel.png" width="24" height="20" style="margin:6px 5px -4px 0;"> <%=AppSettings("mobile")%> <br>
<img src="images/icn_tel.png" width="24" height="20" style="margin:6px 5px -4px 0;"> <%=AppSettings("phone")%> <br>
                <img src="images/icn_fax.png" width="24" height="24" style="margin:6px 5px -4px 0;"> <%=AppSettings("fax")%> 
            <div class="ClearFoat">
            </div>
            </div>
            <div id="ColForm">
                <h1><img src="images/txt_form.png" width="121" height="20"></h1>
                
                <div id="showForm">                 
                    <input name="txtname" runat=server  type="text" class="Txtbox" id="txtname" onblur="if(this.value==&#39;&#39;)this.value=&#39;ชื่อ - นามสกุล&#39;;" onclick="if(this.value==&#39;ชื่อ - นามสกุล&#39;)this.value=&#39;&#39;;" value="ชื่อ - นามสกุล">
                    <input name="txtaddress" runat=server  type="text" class="Txtbox" id="txtaddress" onblur="if(this.value==&#39;&#39;)this.value=&#39;ที่อยู่&#39;;" onclick="if(this.value==&#39;ที่อยู่&#39;)this.value=&#39;&#39;;" value="ที่อยู่">
                    <input name="txttelephone" runat=server  type="text" class="Txtbox" id="txttelephone" onblur="if(this.value==&#39;&#39;)this.value=&#39;เบอร์โทรศัพท์&#39;;" onclick="if(this.value==&#39;เบอร์โทรศัพท์&#39;)this.value=&#39;&#39;;" value="เบอร์โทรศัพท์">
                    <input name="txtemail" runat=server  type="text" class="Txtbox" id="txtemail" onblur="if(this.value==&#39;&#39;)this.value=&#39;อีเมล์&#39;;" onclick="if(this.value==&#39;อีเมล์&#39;)this.value=&#39;&#39;;" value="อีเมล์">
 
                    <textarea name="txtmessage" runat=server  onfocus="if(this.value==&#39;ข้อความ&#39;)this.value=&#39;&#39;" id="txtmessage" cols="45" rows="3" class="Txtbox-area" onblur="if(this.value==&#39;&#39;)this.value=&#39;ข้อความ&#39;;" onclick="if(this.value==&#39;ข้อความ&#39;)this.value=&#39;&#39;;" value="ข้อความ"></textarea>
                    <div class="ClearFoat"></div>
                    <div style="margin:10px 0;" class="comment"> <label  id=lbl_random_number runat=server style="font-weight:bold; color:#d88946" ></label> (ป้องกันสแปม)</a>
                   
                    </div>
                    <input name="txtcode" runat=server  type="text" class="Txtbox" id="txtcode" 
                        onblur="if(this.value==&#39;&#39;)this.value=&#39;กรุณาใส่ผลลัพธ์ด้านบน&#39;;" 
                        onclick="if(this.value==&#39;กรุณาใส่ผลลัพธ์ด้านบน&#39;)this.value=&#39;&#39;;" value="กรุณาใส่ผลลัพธ์ด้านบน" >
                    <div class="ClearFoat"></div>
                    <div style="margin:5px 0;">

                    <input name="button" runat=server  type="submit" class="Btn" id="button"   value="ตกลง">
                    <input name="button2" type="reset" class="Btn" id="button2" value="ยกเลิก" onclick="getReset();"> <label runat=server id=lbl_status class="lbl_status"></label>
                        <div style="padding-left:15px; " id="showLoad">
                                               <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID=udp_contact DisplayAfter=3 runat="server">
                            <ProgressTemplate>
                                <asp:Image ID=img_UpdatePanel1 runat=server ImageUrl="~/images/progress.gif" /> Loading..
                            </ProgressTemplate>
                        </asp:UpdateProgress>   
                        </div>
                    </div>              
                </div> 
            </div>  
        </div>
      </div>
<span   style="position:relative; top:-520px; left:820px; z-index:10000 ">
   
</span>
</ContentTemplate>
</asp:UpdatePanel> 
</asp:Content>
