﻿<%@ page title="LOG-ON" language="VB" masterpagefile="~/Site.Master" autoeventwireup="false" inherits="Member, App_Web_ngygm4mk" debug="true" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        #showForm
        {
            width: 298px;
        }
        .ddl {font-family:Tahoma, Geneva, sans-serif; font-size:12px;  margin-left:-0px; margin-bottom:5px; width:281px; padding:4px; padding-left:0px!important; color:#666;}
        
        .invisible {
            display:none;
        }
        .ratingBox {
            width: 14px;
            height: 13px;
            margin: 0px 0px;
            padding: 0px;
            display: block;
            float: left;
            text-align:center;
            overflow:hidden;
        }
        .ratingStar {
            font-size: 0pt;
            width: 14px;
            height: 13px;
            margin: 0px 0px;
            padding: 0px;
            cursor: pointer;
            display: block;
            float: left;
            background-repeat: no-repeat;
        }
        .filledRatingStar {
            background-image: url(images/star1.png);
        }

        .emptyRatingStar {
            background-image: url(images/star_no.png);
        }
  
    </style>

    <script language="javascript" type="text/javascript">

    </script>
</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent" >

<asp:UpdatePanel ID="udp_member"  runat="server"   RenderMode="Inline" >

<ContentTemplate> 
    <div id="Container">
    <div class="BgContent">
         <asp:MultiView ID=multiview1 runat=server>    
            <asp:View ID=view0 runat=server >     
                <div id="NavAbout">
                    <ul>           
                        <li><asp:HyperLink id="nav_about1_0" runat=server NavigateUrl="~/Member.aspx?tab=1" ><img src="images/member/mn_about_de_01.gif" name="navabout1" border="0" id="navabout1" /></asp:HyperLink></li>
                        <li><asp:HyperLink id="nav_about2_0" runat=server NavigateUrl="~/Member.aspx?tab=2" ><img src="images/member/mn_about_02.gif" name="navabout2" border="0" id="navabout2" onmouseover="this.src=&#39;images/member/mn_about_over_02.gif&#39;"  onmouseout="this.src=&#39;images/member/mn_about_02.gif&#39;"></asp:HyperLink></li>
                        <li><asp:HyperLink id="nav_about3_0" runat=server NavigateUrl="~/Member.aspx?tab=3" ><img src="images/member/mn_about_03.gif" name="navabout3" border="0" id="navabout3" onmouseover="this.src=&#39;images/member/mn_about_over_03.gif&#39;" onmouseout="this.src=&#39;images/member/mn_about_03.gif&#39;"></asp:HyperLink></li>           
                    </ul>
                    <div class="ClearFoat"></div>
                </div> 
                <div id="ColAddress">   
                    <div><br /><br /><label runat=server id=lbl_status class="lbl_status"></label></div>
                    <div class="ClearFoat"></div>
                </div>
                <div id="ColForm" >                                         
                    <div id="showForm">  
                    <h1><img src="images/lbl_user_email.png" width="121" height="20"></h1>                
                    <input name="txt_user_id" runat=server  type="text" class="Txtbox" id="txt_user_id" 
                            value=""  >&nbsp;
                    <h1><img src="images/lbl_user_password.png" width="121" height="20"></h1> 
                    <input name="txt_user_password" runat=server  type="text" class="Txtbox" 
                            id="txt_user_password" value="" >&nbsp;
                    
                    <div class="ClearFoat"></div>
                    <div style="margin:5px 0;">
                  <asp:UpdateProgress  ID="UpdateProgress0" AssociatedUpdatePanelID=udp_member DisplayAfter=3 runat="server">
                    <ProgressTemplate >
                        <asp:Image ID=img_UpdatePanel1 runat=server ImageUrl="~/images/progress.gif" /> Loading..
                    </ProgressTemplate>
                    </asp:UpdateProgress>   
                    <input name="button" runat=server  type="submit" class="Btn" id="button" value="ตกลง">
                    <input name="button2" type="reset" class="Btn" id="button2" value="ยกเลิก" > <asp:CheckBox ID=chk_remember runat=server  CssClass="lbl_status" Text="Remember Me" />
                       <div style="padding-left:15px; " id="showLoad">
                       </div>
                    </div>              
                </div>                 
            </div>  
                <div id="ColAddress"><div class="ClearFoat"></div></div>
            </asp:View>    
            <asp:View ID=view1 runat=server >
                <div id="NavAbout">
                    <ul>           
                        <li><asp:HyperLink id="nav_about1_1" runat=server NavigateUrl="~/Member.aspx?tab=1" ><img src="images/member/mn_about_de_01.gif" name="navabout1" border="0" id="navabout1" /></asp:HyperLink></li>
                        <li><asp:HyperLink id="nav_about2_1" runat=server NavigateUrl="~/Member.aspx?tab=2" ><img src="images/member/mn_about_02.gif" name="navabout2" border="0" id="navabout2" onmouseover="this.src=&#39;images/member/mn_about_over_02.gif&#39;" onmouseout="this.src=&#39;images/member/mn_about_02.gif&#39;"></asp:HyperLink> </li>
                        <li><asp:HyperLink id="nav_about3_1" runat=server NavigateUrl="~/Member.aspx?tab=3" ><img src="images/member/mn_about_03.gif" name="navabout3" border="0" id="navabout3" onmouseover="this.src=&#39;images/member/mn_about_over_03.gif&#39;" onmouseout="this.src=&#39;images/member/mn_about_03.gif&#39;"></asp:HyperLink></li>           
                    </ul>
                    <div class="ClearFoat"></div>
                </div> 
                <div id="ColAddress">   
                    <div><br /><br /><label runat=server id=lbl_status1 class="lbl_status"></label></div>
                    <div class="ClearFoat"></div>
                </div>
                <div id="ColForm">                                                       
                    <div id="showForm" style="width:900px!important; height:60px!important">  
                        <div style="font-weight:bold;   text-transform:capitalize!important; color:#dedede; text-shadow: 0.1em 0.1em 0.2em black"><%=Request.Cookies("member_info")("SiteName")%></div>
                        <%=System.Text.Encoding.Unicode.GetString(Convert.FromBase64String(Request.Cookies("member_info")("uName")))%> | <%=Request.Cookies("member_info")("uDepartment")%>&nbsp;<asp:ImageButton ImageUrl="~/images/edit_info_32.png" ID=btn_member_in_0_edit runat=server  ToolTip =แก้ไขข้อมูลส่วนตัว /> <br />
                        id: <%=Request.Cookies("member_info")("uChildId")%>
                        <table style="position:relative; left:320px; top:-66px;  height:-10px;  width:500px">
                            <tr><td style="font-weight:bold;  color:White; font-size:12pt; text-align:left;"><asp:Label ID=lbl_QuestionTitle runat=server></asp:Label></td></tr>
                            <tr><td style="font-size:10pt; text-align:left; color:darkgray;"><asp:Label ID=lbl_QuestionDescription runat=server Width=500px Height=30px Text="" /></td></tr>
                        </table>  
                        <div class="ClearFoat"></div> 
<%--                        <div style="margin:5px 0;">
                            <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID=udp_member DisplayAfter=3 runat="server">
                                <ProgressTemplate>
                                <asp:Image ID=img_UpdatePanel1 runat=server ImageUrl="~/images/progress.gif" /> Loading..
                                </ProgressTemplate>
                            </asp:UpdateProgress>   
                            <div style="padding-left:15px; " id="showLoad">                       
                            </div>
                        </div>   --%>           
                    </div>  
                    
                </div>  
                <div id="ColAddress"><div class="ClearFoat"></div></div>                
                <div id="ColForm">  
                
                    <div id="showForm">
                        <asp:MultiView ID=mul_member_info runat=server> <%--0 Admin insert user, 1 Users insert query,2 Amdin or Users edit info --%>
                            <%--แอดมินใส่ Users --%>
                            <asp:View ID=viw_member_info_0 runat=server>                             
                                <table >
                                <tr >
                                <%--แอดมินใส่ Users --%>
                                <td  valign=top >   
                                <h1><img src="images/lbl_insert_form.png" width="121" height="20" title="สำหรับแอดมินใช้สร้างยูสเซอร์ของบริษัท"></h1> 
                                    <input name="txt_uDepartment" runat=server  type="text" class="Txtbox" id="txt_uDepartment" onblur="if(this.value==&#39;&#39;)this.value=&#39;แผนก&#39;;" onclick="if(this.value==&#39;แผนก&#39;)this.value=&#39;&#39;;" value="แผนก">
                                    <input name="txt_uChildId" runat=server  type="text" class="Txtbox" id="txt_uChildId" onblur="if(this.value==&#39;&#39;)this.value=&#39;อีเมล์ล๊อกอิน&#39;;" onclick="if(this.value==&#39;อีเมล์ล๊อกอิน&#39;)this.value=&#39;&#39;;" value="อีเมล์ล๊อกอิน">                                
                                    <input name="txt_uName" runat=server  type="text" class="Txtbox" id="txt_uName" onblur="if(this.value==&#39;&#39;)this.value=&#39;ชื่อ - นามสกุล&#39;;" onclick="if(this.value==&#39;ชื่อ - นามสกุล&#39;)this.value=&#39;&#39;;" value="ชื่อ - นามสกุล">
                                    <input name="txt_uTelOffice" runat=server  type="text" class="Txtbox" id="txt_uTelOffice" onblur="if(this.value==&#39;&#39;)this.value=&#39;เบอร์ติดต่อ (บริษัท)&#39;;" onclick="if(this.value==&#39;เบอร์ติดต่อ (บริษัท)&#39;)this.value=&#39;&#39;;" value="เบอร์ติดต่อ (บริษัท)">
                                    <input name="txt_uMobile" runat=server  type="text" class="Txtbox" id="txt_uMobile" onblur="if(this.value==&#39;&#39;)this.value=&#39;เบอร์ติดต่อ (มือถือ)&#39;;" onclick="if(this.value==&#39;เบอร์ติดต่อ (มือถือ)&#39;)this.value=&#39;&#39;;" value="เบอร์ติดต่อ (มือถือ)">                               
                                    <input name="txt_uMemo" runat=server  type="text" class="Txtbox" id="txt_uMemo" onblur="if(this.value==&#39;&#39;)this.value=&#39;หมายเหตุ&#39;;" onclick="if(this.value==&#39;หมายเหตุ&#39;)this.value=&#39;&#39;;" value="หมายเหตุ">
                                    <div style="margin:5px 0;">   
                                        <input name="btn_member_info_0" runat=server  type="submit" class="Btn" id="btn_member_info_0"   value="ตกลง">
                                        <input name="btn_member_info_0_reset" type=button   runat=server   class="Btn" id="btn_member_info_0_reset" value="ยกเลิก"> 
                                        <label runat=server id=lbl_status_member_info_0 class="lbl_status"></label>
                                        <asp:UpdateProgress ID="UpdateProgress4" AssociatedUpdatePanelID=udp_member DisplayAfter=3 runat="server">
                                            <ProgressTemplate>
                                                <asp:Image ID=img_UpdatePanel4 runat=server ImageUrl="~/images/progress.gif" /> Loading..
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>    
                                    </div>
                                </td>
                                <%--ตารางสำหรับดูและลบ Users --%>
                                <td valign=top style="padding-left:30px;">   
                                    <img src="images/lbl_all_users.png" >
                                    <asp:Panel Height=250px  ID=panel_member_info_0 runat=server ScrollBars=Vertical>
                                        <asp:GridView ID=gv_member_info_0 runat=server EnableModelValidation="True" ShowHeader=true  AutoGenerateColumns=false Width=520px   >
                                            <EmptyDataTemplate>ยังไม่มีข้อมูล...</EmptyDataTemplate>
                                            <Columns>
                                                <asp:BoundField DataField="uName" HeaderText="Name" />
                                                <asp:BoundField DataField="uChildID" HeaderText="Login ID" />                                   
                                                <asp:BoundField DataField="uDepartment" HeaderText="Department" />  
                                                <asp:BoundField DataField="uLastLogin" HeaderText="Last Login" />
                                                <asp:CommandField HeaderText="DEL" ShowDeleteButton="True" />
                                            </Columns>                                        
                                        </asp:GridView>
                                    </asp:Panel>
                                </td>
                                </tr>
                                </table> 
                                <div class="ClearFoat"></div>                                   
                            </asp:View> 
                    </div>
                            
                            <%--ผู้ใช้ สร้างคำถาม --%>
                            <asp:View ID=viw_member_info_1 runat=server>
                                <table >
                                <tr >
                                <td  valign=top >   
                                <h1><img src="images/lbl_question_form.png" width="121" height="20"></h1> 

                                    <asp:DropDownList   CssClass="ddl" ID=ddl_question runat=server></asp:DropDownList>
                                    <input name="txt_qQuestionTitle" runat=server  type="text" class="Txtbox" id="txt_qQuestionTitle" onblur="if(this.value==&#39;&#39;)this.value=&#39;หัวข้อ...&#39;;" onclick="if(this.value==&#39;หัวข้อ...&#39;)this.value=&#39;&#39;;" value="หัวข้อ...">
                                    <textarea name="txt_qQuestionDescription" runat=server  onfocus="if(this.value==&#39;รายละเอียด...&#39;)this.value=&#39;&#39;" id="txt_qQuestionDescription" cols="45" rows="7" style="width:270px!important"     class="Txtbox-area" onblur="if(this.value==&#39;&#39;)this.value=&#39;รายละเอียด...&#39;;" onclick="if(this.value==&#39;รายละเอียด...&#39;)this.value=&#39;&#39;;" value="รายละเอียด..."></textarea>
                                    <div style="margin:5px 0;">   
                                        <input name="btn_member_info_1" runat=server  type="submit" class="Btn" id="btn_member_info_1"   value="ตกลง">
                                        <input name="btn_member_info_1_reset" type=button   runat=server   class="Btn" id="btn_member_info_1_reset" value="ยกเลิก"> 
                                        <label runat=server id=lbl_status_member_info_1 class="lbl_status"></label>
                                        <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID=udp_member DisplayAfter=3 runat="server">
                                            <ProgressTemplate>
                                                <asp:Image ID=img_UpdatePanel4 runat=server ImageUrl="~/images/progress.gif" /> Loading..
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>    
                                    </div>
                                </td>
                                <td valign=top style="padding-left:30px;">   <%--ตารางสำหรับ Users ดูคำถาม  --%>
                                    <asp:MultiView ID=mul_user_info_1_a runat=server>
                                    <%--ตารางสำหรับ Users ดูคำถาม (topic)  --%>
                                    <asp:View ID=viw_user_info_1_a_0 runat=server> 
                                        <img src="images/lbl_all_questions.png" >
                                        <asp:Panel Height=250px  ID=panel_member_info_1 runat=server ScrollBars=Vertical>
                                            <asp:GridView  RowStyle-VerticalAlign=Top ID=gv_member_info_1   runat=server EnableModelValidation="True" ShowHeader=true  AutoGenerateColumns=false Width=520px   >
                                                <EmptyDataTemplate>ยังไม่มีข้อมูล...</EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField Visible=false >
                                                        <ItemTemplate>
                                                            <asp:Label ID=lbl_qQuestionID runat=server Text='<%#Eval("qQuestionID")%>'></asp:Label>
                                                            <asp:Label ID=lbl_qCreateDate runat=server Text='<%#Eval("qLastModifyDate")%>'></asp:Label>
                                                            <asp:Label ID=lbl_qQuestionDescription runat=server Text='<%#Eval("qQuestionDescription")%>'></asp:Label>
                                                              <asp:Label ID=lbl_qStatus runat=server Text='<%#Eval("qStatus")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <img id=img_status runat=server src="~/images/lock_16.png" visible=false  />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="qQuestionID"  HeaderText="No." ItemStyle-HorizontalAlign=Justify   DataFormatString="{0:00000}"    ItemStyle-Width=45px  />
                                                    <asp:BoundField DataField="Description" HeaderText="Types" />                                   
                                                    <asp:BoundField DataField="qQuestionTitle" HeaderText="Topics" ItemStyle-Width=290px />  
                                                    <asp:BoundField DataField="qLastModifyDate" HeaderText="Date"  DataFormatString="{0:dd/MM/yy hh:mm}" />
                                                    <asp:CommandField ShowSelectButton=True ButtonType="Image"  
                                                        SelectImageUrl ="~/Images/go_red_16.ico" >
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:CommandField> 
                                                </Columns>                                        
                                            </asp:GridView>
                                        </asp:Panel>                                    
                                    </asp:View>
                                    <%--ตารางสำหรับ Users ดูคำถาม (detail)  --%>
                                    <asp:View ID=viw_user_info_1_a_1 runat=server  > 
                                    <span style="color:white;  "></span><%=ViewState("qQuestionDescription")%>
                                        <asp:Panel ID=panel_user_info_1_a_1 runat=server Height=245px Width=550px ScrollBars=Auto >
                                            <asp:DataList ID="dtl_user_info_1_a_1" runat="server" Width=500px  ShowFooter=true  CellPadding=8  >                          
                                                <ItemStyle   BorderColor=gray BorderWidth=1 BorderStyle=Dotted/>
                                                <ItemTemplate>
                                                    <span style="color:gray;font-size:11pt!important; ">#<%#Container.DataItem("rank_no")%></span><span style="color:darkgray"><%#IIf(InStr(Container.DataItem("uName"), " ") = 0, Container.DataItem("uName") & " ", Container.DataItem("uName").ToString.Substring(0, InStr(Container.DataItem("uName"), " ")))%></sapn><span style="color:grey;" ><%# Container.DataItem("sSolveCreateDate")%><ajaxToolkit:Rating ID="ajRate" runat="server"    CurrentRating="0"    MaxRating="5"    StarCssClass="ratingStar"    WaitingStarCssClass="savedRatingStar"    FilledStarCssClass="filledRatingStar"    EmptyStarCssClass="emptyRatingStar"     />                                                    
                                                    </span>                                                    
                                                    <br  /><span style="color:white;font-size:10pt!important; " ><%#Container.DataItem("sSolveDescription")%></span>
                                                </ItemTemplate>                                    
                                                <FooterTemplate>
                                                    <asp:Label ID="lbl_footer"   runat="server"  />                                    
                                                </FooterTemplate>
                                            </asp:DataList>
                                        </asp:Panel>
                                        <div style="padding-top:8px; "> 
                                            <input name="txt_user_info_1_a_1_message" runat=server  type="text" class="Txtbox" id="txt_user_info_1_a_1_message" onblur="if(this.value==&#39;&#39;)this.value=&#39;สอบถามเพิ่มเติม..&#39;;" onclick="if(this.value==&#39;สอบถามเพิ่มเติม..&#39;)this.value=&#39;&#39;;" value="สอบถามเพิ่มเติม.."> <input name="btn_user_info_1_a_1_message" runat=server  type="submit" class="Btn" id="btn_user_info_1_a_1_message"   value="ส่งข้อความ">
                                            <input name="btn_user_info_1_a_1_message_reset" type=button   runat=server   class="Btn" id="btn_user_info_1_a_1_message_reset" value="ยกเลิก">
                                        </div>
                                    </asp:View>
                                    </asp:MultiView>
                                </td>
                                </tr>
                                </table> 
                                                                                               
                            </asp:View>  
                            <%-- Edit Info --%>
                            <asp:View ID=viw_member_info_2 runat=server> 
                            <h1><img src="images/lbl_edit_your_info.png" width="121" height="20"></h1> 
                                    <table width=450px>
                                        <tr align=left>
                                            <td>แผนก</td><td ><input name="txt_uDepartment_edit" runat=server  type="text" class="Txtbox" id="txt_uDepartment_edit" ></td>
                                        </tr>
                                        <tr>
                                            <td>ชื่อ - นามสกุล</td><td><input name="txt_uName_edit"  runat=server  type="text" class="Txtbox" id="txt_uName_edit"  ></td>
                                        </tr>
                                        <tr>
                                            <td>เบอร์ติดต่อ (บริษัท)</td><td><input name="txt_uTelOffice_edit"  runat=server  type="text" class="Txtbox" id="txt_uTelOffice_edit" ></td>
                                        </tr>
                                        <tr>
                                            <td>เบอร์ติดต่อ (มือถือ)</td><td><input name="txt_uMobile_edit" runat=server  type="text" class="Txtbox" id="txt_uMobile_edit" ></td>
                                        </tr>
                                        <tr>
                                            <td>หมายเหตุ</td><td><input name="txt_uMemo_edit"  runat=server  type="text" class="Txtbox" id="txt_uMemo_edit" ></td>
                                        </tr>
                                        <tr>
                                            <td>เปลี่ยนพาสเวิร์ดใหม่</td><td><input name="txt_uPassword_new_edit" runat=server  type=password class="Txtbox" id="txt_uPassword_new_edit" /> </td>
                                        </tr>
                                        <tr>
                                            <td>พาสเวิร์ดปัจจุบัน</td><td><input name="txt_uPassword_edit" runat=server  type=password  class="Txtbox" id="txt_uPassword_edit" ></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <div style="margin:5px 0;">   
                                                    <input name="btn_member_info_2" runat=server  type="submit" class="Btn" id="btn_member_info_2"   value="ตกลง">
                                                    <input name="btn_member_info_2_reset" type=button   runat=server   class="Btn" id="btn_member_info_2_reset" value="ยกเลิก"> 
                                                    <label runat=server id=lbl_status_member_info_2 class="lbl_status"></label>
                                                    <asp:UpdateProgress ID="UpdateProgress_2" AssociatedUpdatePanelID=udp_member DisplayAfter=3 runat="server">
                                                        <ProgressTemplate>
                                                            <asp:Image ID=img_UpdatePanel_2 runat=server ImageUrl="~/images/progress.gif" /> Loading..
                                                        </ProgressTemplate>
                                                    </asp:UpdateProgress>    
                                                </div>                                            
                                            </td>
                                        </tr>
                                    </table>
                            </asp:View>                                            
                            <%--4 OPS ตอบคำถาม Main--%>
                            <asp:View ID=viw_member_info_3 runat=server>
                                <table >
                                <tr >
                                  <%--OPS 4.1 ค้นคำถาม --%>
                                <td  valign=top >   
                                <h1><img src="images/lbl_question_filter.png" width="121" height="20"></h1>                                
                                    <asp:DropDownList   CssClass="ddl" ID=ddl_Question_reply runat=server></asp:DropDownList>
                                    <asp:DropDownList   CssClass="ddl" ID=ddl_Site_reply runat=server></asp:DropDownList>
                                    <input name="txt_qQuestionID_reply" runat=server  type="text" class="Txtbox" id="txt_qQuestionID_reply" onblur="if(this.value==&#39;&#39;)this.value=&#39;เลขที่คำถาม...&#39;;" onclick="if(this.value==&#39;เลขที่คำถาม...&#39;)this.value=&#39;&#39;;" value="เลขที่คำถาม...">
                                    <input name="txt_qQuestionTitle_reply" runat=server  type="text" class="Txtbox" id="txt_qQuestionTitle_reply" onblur="if(this.value==&#39;&#39;)this.value=&#39;หัวข้อคำถาม...&#39;;" onclick="if(this.value==&#39;หัวข้อคำถาม...&#39;)this.value=&#39;&#39;;" value="หัวข้อคำถาม...">
                                    
                                    <div style="margin:5px 0;">   
                                        <input name="btn_member_info_3" runat=server  type="submit" class="Btn" id="btn_member_info_3"   value="ตกลง">
                                        <input name="btn_member_info_3_reset" type=button   runat=server   class="Btn" id="btn_member_info_3_reset" value="ยกเลิก"> 
                                        <label runat=server id=lbl_status_member_info_3 class="lbl_status"></label>
                                        <asp:UpdateProgress ID="UpdateProgress5" AssociatedUpdatePanelID=udp_member DisplayAfter=3 runat="server">
                                            <ProgressTemplate>
                                                <asp:Image ID=img_UpdatePanel4 runat=server ImageUrl="~/images/progress.gif" /> Loading..
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>    
                                    </div>
                                </td>
                                <td valign=top style="padding-left:30px;"> 
                                    <asp:MultiView ID=mul_user_info_2_a runat=server>
                                     <%--4.2 OPS แสดงตารางคำถาม --%>
                                    <asp:View ID=viw_user_info_2_a_0 runat=server> 
                                        <img src="images/lbl_all_questions.png" >
                                        <asp:Panel Height=250px  ID=panel_member_info_3 runat=server ScrollBars=Vertical>
                                            <asp:GridView  RowStyle-VerticalAlign=Top ID=gv_member_info_3   runat=server EnableModelValidation="True" ShowHeader=true  AutoGenerateColumns=false Width=520px   >
                                                <EmptyDataTemplate>ยังไม่มีข้อมูล...</EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField Visible=false >
                                                        <ItemTemplate>
                                                            <asp:Label ID=lbl_qQuestionID_reply runat=server Text='<%#Eval("qQuestionID")%>'></asp:Label>
                                                            <asp:Label ID=lbl_qCreateDate_reply runat=server Text='<%#Eval("qLastModifyDate")%>'></asp:Label>
                                                            <asp:Label ID=lbl_qQuestionDescription_reply runat=server Text='<%#Eval("qQuestionDescription")%>'></asp:Label>
                                                            <asp:Label ID=lbl_qChildId_reply runat=server Text='<%#Eval("qChildId")%>'></asp:Label>
                                                            <asp:Label ID=lbl_qQuestionTitle_reply runat=server Text='<%#Eval("qQuestionTitle")%>'></asp:Label>
                                                            <asp:Label ID=lbl_qStatus_reply runat=server Text='<%#Eval("qStatus")%>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <img id=img_status_reply runat=server src="~/images/lock_16.png" visible=false  />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="qQuestionID"  HeaderText="No." ItemStyle-HorizontalAlign=Justify   DataFormatString="{0:00000}"    ItemStyle-Width=45px  />
                                                    <asp:BoundField DataField="Description" HeaderText="Types" />                                   
                                                    <asp:BoundField DataField="qQuestionTitle" HeaderText="Topics" ItemStyle-Width=290px />  
                                                    <asp:BoundField DataField="qLastModifyDate" HeaderText="Date"  DataFormatString="{0:dd/MM/yy hh:mm}" />
                                                    <asp:CommandField ShowSelectButton=True ButtonType="Image"  
                                                        SelectImageUrl ="~/Images/go_red_16.ico" >
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:CommandField> 
                                                </Columns>                                        
                                            </asp:GridView>
                                        </asp:Panel>                                    
                                    </asp:View>
                                    <%--ตารางสำหรับ OPS ดูคำถาม (detail)  --%>
                                    <asp:View ID=viw_user_info_2_a_1 runat=server  > 
                                    <span style="color:white;  "></span><%=ViewState("qQuestionDescription")%>
                                        <asp:Panel ID=panel_user_info_2_a_1 runat=server Height=245px Width=550px ScrollBars=Auto >
                                            <asp:DataList ID="dtl_user_info_2_a_1" runat="server" Width=500px  ShowFooter=true  CellPadding=8  >                          
                                                <ItemStyle   BorderColor=gray BorderWidth=1 BorderStyle=Dotted/>
                                                <ItemTemplate>
                                                    <span style="color:gray;font-size:11pt!important; ">#<%#Container.DataItem("rank_no")%></span><span style="color:darkgray"><%#IIf(InStr(Container.DataItem("uName"), " ") = 0, Container.DataItem("uName") & " ", Container.DataItem("uName").ToString.Substring(0, InStr(Container.DataItem("uName"), " ")))%></sapn><span style="color:grey;" ><%# Container.DataItem("sSolveCreateDate")%><ajaxToolkit:Rating ID="ajRate" runat="server"    CurrentRating="0"    MaxRating="5"    StarCssClass="ratingStar"    WaitingStarCssClass="savedRatingStar"    FilledStarCssClass="filledRatingStar"    EmptyStarCssClass="emptyRatingStar"     />                                                    
                                                    </span>                                                    
                                                    <br  /><span style="color:white;font-size:10pt!important; " ><%#Container.DataItem("sSolveDescription")%></span>
                                                </ItemTemplate>                                    
                                                <FooterTemplate>
                                                    <asp:Label ID="lbl_footer_reply"   runat="server"  />                                    
                                                </FooterTemplate>
                                            </asp:DataList>
                                        </asp:Panel>
                                        <div style="padding-top:8px;  vertical-align:middle "> 
                                            <input name="txt_user_info_2_a_1_message" runat=server   type="text" class="Txtbox" id="txt_user_info_2_a_1_message" onblur="if(this.value==&#39;&#39;)this.value=&#39;ตอบคำถาม..&#39;;" onclick="if(this.value==&#39;ตอบคำถาม..&#39;)this.value=&#39;&#39;;" value="ตอบคำถาม.."> 
                                            <input name="btn_user_info_2_a_1_message" runat=server  type="submit" class="Btn" id="btn_user_info_2_a_1_message"   value="ส่งข้อความ">
                                            <input name="btn_user_info_2_a_1_message_reset" type=button   runat=server   class="Btn" id="btn_user_info_2_a_1_message_reset" value="ยกเลิก">&nbsp;&nbsp;&nbsp;
                                            <asp:ImageButton ID="btn_user_info_2_a_1_message_delete" ImageAlign=AbsMiddle   runat=server ImageUrl="~/images/del_16.png" /> | <asp:ImageButton ID="btn_user_info_2_a_1_message_close" runat=server ImageAlign=AbsMiddle  ImageUrl="~/images/lock_16.png" />
                                        </div>
                                    </asp:View>
                                    </asp:MultiView>
                                </td>
                                </tr>
                                </table> 
                            </asp:View>
                        </asp:MultiView>
                    </div>
                </div>

            </asp:View>
            <asp:View ID=view2 runat=server >            
               <div id="NavAbout">
                    <ul>           
                        <li><asp:HyperLink id="nav_about1_2" runat=server NavigateUrl="~/Member.aspx?tab=1" ><img src="images/member/mn_about_01.gif" name="navabout1" border="0" id="navabout1"  onmouseover="this.src=&#39;images/member/mn_about_over_01.gif&#39;" onmouseout="this.src=&#39;images/member/mn_about_01.gif&#39;"></asp:HyperLink></li>
                        <li><asp:HyperLink id="nav_about2_2" runat=server NavigateUrl="~/Member.aspx?tab=2" ><img src="images/member/mn_about_de_02.gif" name="navabout2" border="0" id="navabout2" /></asp:HyperLink></li> 
                        <li><asp:HyperLink id="nav_about3_2" runat=server NavigateUrl="~/Member.aspx?tab=3" ><img src="images/member/mn_about_03.gif" name="navabout3" border="0" id="navabout3" onmouseover="this.src=&#39;images/member/mn_about_over_03.gif&#39;" onmouseout="this.src=&#39;images/member/mn_about_03.gif&#39;"></asp:HyperLink></li>             
                    </ul>
                    <div class="ClearFoat"></div>
                </div> 
                <div id="ColAddress">   
                    <div><br /><br /><label runat=server id=lbl_status2 class="lbl_status"></label></div>
                    <div class="ClearFoat"></div>
                </div>
                <div id="ColForm">                                         
                    <div id="showForm">  
                        <h1><img src="images/lbl_user_email.png" width="121" height="20"></h1>                
                        หน้าโหลด drivers &nbsp;
                                          
                        <div class="ClearFoat"></div>
                        <div style="margin:5px 0;">
                            <asp:UpdateProgress ID="UpdateProgress2" AssociatedUpdatePanelID=udp_member DisplayAfter=3 runat="server">
                                <ProgressTemplate>
                                <asp:Image ID=img_UpdatePanel1 runat=server ImageUrl="~/images/progress.gif" /> Loading..
                                </ProgressTemplate>
                            </asp:UpdateProgress>   
                            <div style="padding-left:15px; " id="showLoad">                       
                            </div>
                        </div>              
                    </div>                 
                </div>  
                <div id="ColAddress"><div class="ClearFoat"></div></div>           
            </asp:View>

            
            <asp:View ID=view3 runat=server >            
               <div id="NavAbout">
                    <ul>           
                        <li><asp:HyperLink id="nav_about1_3" runat=server NavigateUrl="~/Member.aspx?tab=1" ><img src="images/member/mn_about_01.gif" name="navabout1" border="0" id="navabout1"  onmouseover="this.src=&#39;images/member/mn_about_over_01.gif&#39;" onmouseout="this.src=&#39;images/member/mn_about_01.gif&#39;"></asp:HyperLink></li>
                        <li><asp:HyperLink id="nav_about2_3" runat=server NavigateUrl="~/Member.aspx?tab=2" ><img src="images/member/mn_about_02.gif" name="navabout2" border="0" id="navabout2" onmouseover="this.src=&#39;images/member/mn_about_over_02.gif&#39;" onmouseout="this.src=&#39;images/member/mn_about_02.gif&#39;"></asp:HyperLink></li> 
                        <li><asp:HyperLink id="nav_about3_3" runat=server NavigateUrl="~/Member.aspx?tab=3" ><img src="images/member/mn_about_de_03.gif" name="navabout3" border="0" id="navabout3" /></asp:HyperLink></li>             
                    </ul>
                    <div class="ClearFoat"></div>
                </div> 
                <div id="ColAddress">   
                    <div><br /><br /><label runat=server id=lbl_status3 class="lbl_status"></label></div>
                    <div class="ClearFoat"></div>
                </div>
                <div id="ColForm">                                         
                    <div id="showForm">  
                        <h1><img src="images/lbl_user_email.png" width="121" height="20"></h1>                
                        หน้า Q&A &nbsp;
                                          
                        <div class="ClearFoat"></div>
                        <div style="margin:5px 0;">
                            <asp:UpdateProgress ID="UpdateProgress3" AssociatedUpdatePanelID=udp_member DisplayAfter=3 runat="server">
                                <ProgressTemplate>
                                <asp:Image ID=img_UpdatePanel1 runat=server ImageUrl="~/images/progress.gif" /> Loading..
                                </ProgressTemplate>
                            </asp:UpdateProgress>   
                            <div style="padding-left:15px; " id="showLoad">                       
                            </div>
                        </div>              
                    </div>                 
                </div>  
                <div id="ColAddress"><div class="ClearFoat"></div></div>           
            </asp:View>
             
        </asp:MultiView>

        </div>
      </div>



</ContentTemplate>
<Triggers>
<asp:AsyncPostBackTrigger ControlID = "gv_member_info_1" /> 
<asp:AsyncPostBackTrigger ControlID="gv_member_info_3"></asp:AsyncPostBackTrigger>
</Triggers> 
<Triggers>
<asp:AsyncPostBackTrigger ControlID = "gv_member_info_3" /> 
</Triggers> 
</asp:UpdatePanel>
<script language=javascript type="text/javascript">
    //   window.scroll(50, 50);
   
</script>

</asp:Content>