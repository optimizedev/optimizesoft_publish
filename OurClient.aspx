﻿<%@ page title="Our Clients" language="VB" masterpagefile="~/Site.Master" autoeventwireup="false" inherits="OurClient, App_Web_ngygm4mk" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent"  >
    <div id="Container">
    <div class="BgContent" >
    <table>
        <tr valign=top>
            <td>
                <asp:Panel ID=panel1 runat=server ScrollBars=Auto Height="490px"  style="margin-top: 0px" Width="506px" >
                    <table class="tb_grid" >
                        <tr >
                            <td><img src="images/clients/autoarmor.jpg"  onMouseOver="Tip('Auto Armor')" onMouseOut="UnTip()" style="cursor:default" /></td>
                            <td><img src="images/clients/benz.jpg" onMouseOver="Tip('Benz Autoservice, Benz Thanan')" onMouseOut="UnTip()" style="cursor:default" /></td>
                                 <td><img src="images/clients/ford.jpg" onMouseOver="Tip('Ford Rama2')" onMouseOut="UnTip()" style="cursor:default"  /></td>
                              
                            <td><img src="images/clients/topmost.jpg" onMouseOver="Tip('Topmost Auto Imported')" onMouseOut="UnTip()" style="cursor:default"  /></td>
                        </tr>
                        <tr>
                             <td><img src="images/clients/lapaloma.jpg" onMouseOver="Tip('La Paloma')" onMouseOut="UnTip()" style="cursor:default" /></td>
                           
                            <td><img src="images/clients/carmax.jpg" onMouseOver="Tip('Carmax Rama9')" onMouseOut="UnTip()" style="cursor:default" /></td>
                                    <td><img src="images/clients/Hyundai.jpg" onMouseOver="Tip('Hyundai Taksin')" onMouseOut="UnTip()" style="cursor:default" /></td>
                <td><img src="images/clients/brg.jpg"  onMouseOver="Tip('BRG Group')" onMouseOut="UnTip()" style="cursor:default" /></td>

                        </tr> 
                        <tr>
                            <td><img src="images/clients/som.jpg" onMouseOver="Tip('SOM Garage')" onMouseOut="UnTip()" style="cursor:default" /></td>
                            <td><img src="images/clients/nissan.jpg"  onMouseOver="Tip('Nissan Prime')" onMouseOut="UnTip()" style="cursor:default" /></td>
                          
                            <td><img src="images/clients/americana.jpg" onMouseOver="Tip('Americana')" onMouseOut="UnTip()" style="cursor:default"  /></td>
                         
                                 <td><img src="images/clients/tsl.jpg" onMouseOver="Tip('TSL')" onMouseOut="UnTip()" style="cursor:default" /></td>
      
                        </tr>
                        <tr>
                               
                        <td><img src="images/clients/yvp.jpg" onMouseOver="Tip('Y.V.P. Group')" onMouseOut="UnTip()" style="cursor:default" /></td>
                          
                            <td><img src="images/clients/Jms.jpg" onMouseOver="Tip('JMS Thailand')" onMouseOut="UnTip()" style="cursor:default"  /></td>
                 
                           <td><img src="images/clients/benzamorn.jpg" onMouseOver="Tip('Benz Amorn')" onMouseOut="UnTip()" style="cursor:default" /></td>    
      <td><img src="images/clients/setpoint.jpg" onMouseOver="Tip('Set Point')" onMouseOut="UnTip()" style="cursor:default" /></td>
                        </tr>
                        <tr>                           
                            <td><img src="images/clients/wish.jpg" onMouseOver="Tip('Wish Motor')" onMouseOut="UnTip()" style="cursor:default" /></td>
                            <td><img src="images/clients/suzuki.jpg" onMouseOver="Tip('Suzuki Siam Inter')" onMouseOut="UnTip()" style="cursor:default" /></td>
                            <td><img src="images/clients/sns.jpg"  onMouseOver="Tip('Siam Nissan')" onMouseOut="UnTip()" style="cursor:default" /></td>
                            <td><img src="images/clients/Sivadol.jpg" onMouseOver="Tip('Sivadon')" onMouseOut="UnTip()" style="cursor:default" /></td>
                        </tr>
                        <tr>                           
                            <td><img src="images/clients/Mazda_กำแพงเพชร.jpg" onMouseOver="Tip('มาสด้ากำแพงเพชร')" onMouseOut="UnTip()" style="cursor:default" /></td>
                            <td><img src="images/clients/Mitsu_Suvanaphum.jpg" onMouseOver="Tip('มิตซูกำแพงเพชร')" onMouseOut="UnTip()" style="cursor:default" /></td>
                            <td><img src="images/clients/Mitsu_กำแพงเพชร.jpg"  onMouseOver="Tip('มิตซูสุวรรณภูมิ')" onMouseOut="UnTip()" style="cursor:default" /></td>
                            <td><img src="images/clients/Siamnissan_Auto.jpg" onMouseOver="Tip('สยามนิสสันออโต้')" onMouseOut="UnTip()" style="cursor:default" /></td>
                        </tr>

                    </table>
    </asp:Panel>
            </td>
            <td style="padding-left:5px">
                <div id="NavAbout">
                    <ul>
                        <li><img src="images/lbl_our_client.gif" name="navabout1" border="0" id="navabout1"></li>
                    </ul>
                    <div class="ClearFoat"></div>
                    <p style="color:white">
                    ....
                    </p>
                </div> 
            </td>
        </tr>    
    </table>

    
    </div>
</div>
</asp:Content>