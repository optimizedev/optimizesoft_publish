/**
* Fullscreenr - lightweight full screen background jquery plugin
* By Jan Schneiders
* Version 1.0
* www.nanotux.com
**/

//about page//
function slideSwitch_about() {
	var id=$('#ImgCurrent_about').val();
	 $('.ImgHeaderAbout').animate({opacity: 0.0}, 1000,function (){
		 $('.ImgHeaderAbout').css('background','url(images/banner/'+id+'.jpg) no-repeat top center');
	});
	 $('.ImgHeaderAbout').animate({opacity: 1.0}, 1000);
	 if(id==5){var next=1;}else{var next=parseInt(id)+parseInt(1);}
	 $('#ImgCurrent_about').val(next);
}

$(function(){
	setInterval("slideSwitch_about()", 10000);
});
//end about page//

