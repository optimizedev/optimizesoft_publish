﻿<%@ page language="VB" autoeventwireup="false" inherits="admin, App_Web_pqozl1td" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:panel id="pnl_company" runat=server DefaultButton="btn_add_company">
            <table>
                <tr>
                    <td>ชื่อบริษัท </td><td><asp:TextBox ID="txt_siteName" runat="server"></asp:TextBox><br /></td>
                </tr>
                <tr>
                    <td>ที่อยู่ </td><td><asp:TextBox ID="txt_siteAddress1" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td></td><td><asp:TextBox ID="txt_siteAddress2" runat="server"></asp:TextBox><br /></td>
                </tr>
                <tr>
                    <td>จังหวัด</td><td> <asp:TextBox ID="txt_siteProvince" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>รหัสไปรษณีย์ </td><td><asp:TextBox ID="txt_siteZipCode" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>โทร </td><td><asp:TextBox ID="txt_siteTelOffice" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>มือถือ </td><td>    <asp:TextBox ID="txt_siteMobile1" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>มือถือ </td><td><asp:TextBox ID="txt_siteMobile2" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>แฟกซ์ </td><td><asp:TextBox ID="txt_siteFax" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>บันทึก </td><td><asp:TextBox ID="txt_siteMemo" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td><asp:label ID=lbl_status_company runat=server Text=""></asp:label></td><td align="right"><asp:Button ID="btn_add_company" runat=server Text="เพิ่มบริษัท" /></td>
                </tr>
            </table>                     
            
            <br />
            
        </asp:panel>
        <asp:Panel ID="pnl_employee" runat=server DefaultButton="btn_add_employee">
            <table>
            <tr> <%--fix uUserType=e, uDepartment=Admin--%>
                <td>บริษัท </td><td><asp:DropDownList ID=ddl_site runat=server ></asp:DropDownList></td>
            </tr>
            <tr>
                <td>ชื่อ-นามสกุล </td><td><asp:TextBox ID="txt_uName" runat="server"/> </td>
            </tr>
            <tr>
                <td>อีเมล์ </td><td><asp:TextBox ID="txt_uChildId" runat="server" /></td>
            </tr>
            <tr>
                <td>รหัส </td><td><asp:TextBox ID="txt_uPassword" runat="server" /></td>
            </tr>
            <tr>
                <td>โทร </td><td><asp:TextBox ID="txt_uTelOffice"  runat="server" /> </td>
            </tr>
            <tr>
                <td>มือถือ </td><td><asp:TextBox ID="txt_uMobile" runat="server" /> </td>
            </tr>
            <tr>
                <td>บันทึก </td><td><asp:TextBox ID="txt_uMemo" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td><asp:label ID=lbl_status_employee runat=server Text=""></asp:label></td><td align="right"><asp:Button ID="btn_add_employee" runat=server Text="เพิ่มพนักงาน" />
        </td>
            </tr>
            </table>        
        </asp:Panel>
    </div>
    </form>
</body>
</html>
